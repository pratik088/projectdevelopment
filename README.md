# ProjectDevelopment

Instructions on how to use the project.

1. Clone the project using git clone https://pratik088@bitbucket.org/pratik088/projectdevelopment.git

2. Open the project on any IDE(Eg. Brackets, Visual Studio Code etc.)

3. The project is still in design phase so you can create more css and javascripts related to the website and then commit it.

4. We will look forward to changes and will make the last decision.

Thank you for contributing.


LICENCE : - I used MIT licence because with it you have less restrictions and lots of permissions. The licence is permissive and very short compare to other open source licences.